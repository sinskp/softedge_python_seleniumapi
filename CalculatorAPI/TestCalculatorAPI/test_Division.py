import pytest
@pytest.mark.usefixtures("CreateCalculatorObj1")
class TestDivision():
    Calc=None

    def testDivisionWithPositiveValues(self,ProvidePositiveValues):
        ActualResult=TestDivision.Calc.Division(ProvidePositiveValues[0],ProvidePositiveValues[1])
        assert ActualResult==ProvidePositiveValues[2], "Division is not working with positive values"

    @pytest.mark.parametrize("num1, num2, output", [(-10,-2,5),(-20,-10,2),(-30,-3,10)])
    def testDivisionWithNegativeValues(self,num1,num2,output):
        ActualResult=TestDivision.Calc.Division(num1,num2)
        assert ActualResult==output, "Division is not working with negative values"

    def testDivisionWithBiggerPositiveValues(self,ProvideBiggerPositiveValues):
        ActualResult = TestDivision.Calc.Division(ProvideBiggerPositiveValues["num1"],ProvideBiggerPositiveValues["num2"])
        assert ActualResult == ProvideBiggerPositiveValues["ExpectedResult"], "Division is not working with Bigger Positive values"
