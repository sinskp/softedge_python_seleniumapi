'''
    https://gitlab.com/Pranoday/softedgeseleniumpythonbatch1.git
    pytest <filename>
    e.g. pytest test_Addition.py will run test functions from this file only
    pip install pytest-html
    pytest -k POSITIVE -v --> K is to find test run based on functionname
    -v is for verbose tracing label for log details
    pytest -k POSITIVE -v --html=./../ UnitTest3.html
    pytest -k POSITIVE -v --html UnitTest3.html
    making the fixture either method or class level
    Fixture must have a return statement
    While using conftest.py the below syntax to be removed
    import pytest
    Obj = None
    and the @pytest.fixture() section to be commented
    '''
import pytest

from CalculatorAPI.CalculatorAPI import Calculator
Obj = None

def testAdditionWithPositiveValues():
    Obj=Calculator()
    Res=Obj.Addition(10,20)
    assert Res==30, "Addition doesn't work with +ve values"
def testAdditionWithOnePositiveNumbers():
    Obj = Calculator()
    Res = Obj.Addition(100, -20)
    # This testcase function is a driver means it is a dummy which calls
    # Addition. It send it required values and compare the result of Addition operation with
    # expected result using Assertion

    assert Res==80,"Addition does not work with 1 positive and 1 -ve values"

def testAdditionFunctionWithOneNegativeNumbers():

    Obj = Calculator()
    Res = Obj.Addition(-50, -20)
    assert Res == -70,"Addition does not work with both -ve values"

'''
@pytest.fixture()
def CreateCalculatorObj():
    #Obj=Calculator()
    global Obj
    Obj=Calculator()
    yield
    print("Going to execute after every function")
'''

@pytest.mark.RegressionTest
def testAdditionWithPositiveValues(CreateCalculatorObj):
    #Obj=Calculator()
    # The below statement is when using the fixture in this file
    #Res=Obj.Addition(10,20)
    # The below statement is when using conftest.py
    Res=CreateCalculatorObj.Addition(10,20)
    assert Res==30,"Addition does not work with positive values"

@pytest.mark.SanityTest
def testAdditionwithOnePositiveOneNegativeNumbers(CreateCalculatorObj):
    #Obj = Calculator()
    # The below statement is when using the fixture in this file
    #Res = Obj.Addition(100, -20)
    # The below statement is when using conftest.py
    Res = CreateCalculatorObj.Addition(100, -20)
    assert Res == 80, "Addition does not work with one positive and one negative values"

@pytest.mark.Smoktest
def testAdditionFunctionWithNegativeValues(CreateCalculatorObj):
    # Obj = Calculator()
    # The below statement is when using the fixture in this file
    #Res = Obj.Addition(-50, -20)
    # The below statement is when using conftest.py
    Res = CreateCalculatorObj.Addition(-50, -40)
    assert Res == -90, "Addition does not work with one positive and one negative values"