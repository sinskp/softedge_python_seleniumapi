import pytest

from CalculatorAPI.CalculatorAPI import Calculator

@pytest.mark.RegressionTest
def testSubstractionWithPositiveValues():
    Obj=Calculator()
    res=Obj.Subtrcation(50,20)
    assert res==30,"Subtraction doesn't work with positive values"
    print("subtraction test")

@pytest.mark.Sanity
def testSubstractionWithOnePositiveAndOneNegativeValues():
    Obj=Calculator()
    res=Obj.Subtrcation(-50,10)
    assert res==-60,"Subtraction doesn't work with 1 positive and 1 negative values"
