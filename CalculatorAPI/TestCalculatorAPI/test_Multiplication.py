import pytest

from CalculatorAPI.CalculatorAPI import Calculator

@pytest.mark.usefixtures("CreateCalculatorObj1")
class TestMultiplication():
    Calc = None

    def testMultiplicationwithPositiveValues(self):
        res = TestMultiplication.Calc.Multiplication(10, 20)
        # res = Obj.Multiplication(10,20)
        assert 200 == res, "Multiplication does not work with positive values"
    def testMultiplicationWithZero(self):
        res = TestMultiplication.Calc.Multiplication(100, 0)
        # res = Obj.Multiplication(100,0)
        assert 0 == res, "Multiplication does not work with positive values"

'''
    Obj=None
    @pytest.fixture()
    def CreateCalculatorObj(self):
        print("In Create Oj")
        TestMultiplication.Obj=Calculator()
        yield
        print("String object to none")
        TestMultiplication.Obj=None
        #global Obj
        #Obj = Calculator()
'''
'''
    def testMultiplicationwithPositiveValues(self):
        res=TestMultiplication.Obj.Multiplication(10,20)
        #res = Obj.Multiplication(10,20)
        assert 200==res,"Multiplication does not work with positive values"

    def testMultiplicationWithZero(self):
        res = TestMultiplication.Obj.Multiplication(100,0)
        #res = Obj.Multiplication(100,0)
        assert 0 == res, "Multiplication does not work with positive values"
'''

