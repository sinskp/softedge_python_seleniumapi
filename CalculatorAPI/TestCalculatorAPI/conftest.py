import pytest

from CalculatorAPI.CalculatorAPI import Calculator

@pytest.fixture()
def CreateCalculatorObj():
    Obj = Calculator()
    return Obj

@pytest.fixture(scope="class")
def CreateCalculatorObj1(request):
    Obj = Calculator()
    request.cls.Calc=Obj

@pytest.fixture(params=[(4,2,2),(99,11,9),(300,3,100)])
def ProvidePositiveValues(request):
    return request.param

@pytest.fixture(params=[{"num1":400,"num2":200,"ExpectedResult":2},{"num1":4000,"num2":200,"ExpectedResult":20},{"num1":400000,"num2":100000,"ExpectedResult":4}])
def ProvideBiggerPositiveValues(request):
    return request.param