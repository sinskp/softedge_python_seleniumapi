nums=[10, 20, 30, 40, 50, 60, 70, 80, 90]
print(nums[0])

Newlist=nums[1:4]
print(Newlist)

'''Req. If we want to update a list with some set of values'''

Newlist1=[1, 2, 3, 4, 5, 6, 7, 8, 9]
Newlist1[0:4:1] = [101, 102, 103, 104]

print(Newlist1)

'''
If we have slicing filter greater than the number of value 
mentioned in right side. I.e. we want to replace the value 
from 0th to 3rd list values, but values assigned only with two values 
[101, 102], then the 2nd and 3rd value will remove from the list.
I.e. here 1 and 2 will be replaced by 101 and 102. Then 3rd and 4th values
i.e. 3 and 4 will be excluded from the new list.
'''
Newlist1=[1, 2, 3, 4, 5, 6, 7, 8, 9]
Newlist1[0:4:1] = [101, 102]
print(Newlist1)

'''
If we have slicing filter lesser than the number of value 
mentioned in right side. I.e. we want to replace the value 
from 0th to 3rd list values, but values assigned only with six values 
[101, 102, 103, 104, 105, 106], then the 4th and 5th index value
i.e. 105 and 106 will be inserted before 7, 8 and 9.
'''
Newlist1=[1, 2, 3, 4, 5, 6, 7, 8, 9]
Newlist1[0:4:1] = [101, 102, 103, 104, 105, 106]
print(Newlist1)


Newlist1=[1, 2, 3, 4, 5, 6, 7, 8, 9]
Newlist1[0:2:1] = []   # 1 and 2 removed here from the list
print(Newlist1)

''' If we want to remove a list of values from the list using del'''
Newlist1=[1, 2, 3, 4, 5, 6, 7, 8, 9]
del Newlist1[2:6]
print(Newlist1)

'''Changing the string value within a LIST, because alone string value is immutable'''
ListOfNames=["Sarat","Kumar","Pradhan"]
ListOfNames[0]="Sorat"
print(ListOfNames)
