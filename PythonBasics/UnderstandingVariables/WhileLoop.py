num1=1
while num1 in range(1, 11):
    print(num1)
    num1=num1+1

lst=[10, 20, 30, 40, 50, 60, 70, 80, 90, 100]
idx=0
print("Accessing elements from list using while loop")
while idx in range(0,len(lst)):
    print(lst[idx])
    idx=idx+1

name="sarat"

idx=0
while idx in range(0,len(name)):
    print(name[idx])
    idx=idx+1

lst=[10, 20, 30, 40, 50, 60, 70, 80, 90, 100]
idx=len(lst)-1
print("printing list in reverse order")
while idx in range(0,len(lst)):
    print(lst[idx])
    idx=idx-1
