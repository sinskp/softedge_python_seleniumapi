Name = "Sarat"
'''
S a r a t
'''
print("First Char of the String",Name[0])
print("Last Char of the String",Name[len(Name)-1])
#Strings are immutable, so we can't change the contents of the string
#Name[0]="P"

Name=" Sarat "
#Strip function remove the leading and trailing spaces
print(Name.strip())
print(Name)

Name=" Sarat Pradhan "
#Strip function remove the leading and trailing spaces and not the in betweeb spaces
print(Name.strip())
print(Name)
print(Name.lstrip())
print(Name.rstrip())

#index starts with counting 0
#rindex search the char from right side
Name=" Sarat Pradhan "
print(Name.index("a"))
print(Name.rindex("a"))
#in the below case, it will start searching char "a" after 6th position
#The print the actual position i.e. 9
print(Name.index("a",6))
print(Name.rindex("a",6))

Name=" Sarat Pradhan "
print(Name.find("Z"))
#print("Last Char of the String",Name[len(Name)+2])
print(Name.find("P"))

Name=" Sarat Pradhan "
#print("Last Char of the String",Name[len(Name)+1])

#Requirement is to remove all the spaces
#Using SPLIT statement
#It contains leading and traling spaces in the LIST
Name=" My name is Sarat Kumar Pradhan "
pieces=Name.split(" ")
print(pieces)
pieces=Name.strip().split(" ")
print(pieces)

pieces=Name.split("a")
print(pieces)

#Replace will not modify the original String
Name=" My name is Sarat Kumar Pradhan "
print(Name.replace("a","A"))
print(Name)

#Adding spaces to a string using padding using center
print(Name.center(40))
#ends with returns true, if string ends with required char else return false
print(Name.endswith("han "))
#Starts with returns true, if sring start with required char else return false
print(Name.strip().startswith("My"))
print(Name.startswith(" My"))







