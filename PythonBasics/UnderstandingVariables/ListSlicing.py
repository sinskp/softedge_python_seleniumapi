Data=["Sarat",10,10.0,True,"Pune","India"]

'''
Element:    Sarat  10  10.0    True    Pune    India
Index:      0       1   2       3       4       5      
'''
'''
Start is inclusive
Stop is exclusive
step is 1 by default

'''
print(Data[0])
#Data[Start:End:Step]
print(Data[0:3])
# 1 for 2nd value. It Will start from 2nd position to 5th char and 6th will be sipped
print(Data[1:5])
#Will start from 2nd position to last
print(Data[1:])
# Will start from 0th 1st position
print(Data[:5])
#display full data
print(Data[:])

print(Data[::2])

#print last 2 elements
print(Data[len(Data)-2:])
print(Data[-2:])
# A value beyond the length lik 1000 or any value will not give any error.
# It is different than Index, where it will give error.
print(Data[9:10])
# Having start value less than stop value also give o value list, if no step value is used as reverse order
print(Data[10:0])

print(Data[4:2])
# Start from 1st index value and pick 2nd value from 1 and pick 2nd value from 3rd
print(Data[1::2])
#start from 1st index and skip 2value and pick 3rd one
print(Data[1::3])

#negative indexing
#Reversing possible by giving -ve step
print(Data[::-1])

Name="Sarat"
print(Name[::-1])

'''
Element:    Sarat  10  10.0    True    Pune    India
Index:      0       1   2       3       4       5  
-ve Index:  -6      -5  -4      -3      -2      -1  
'''

print(Data[4:2:-1])

print(Data[2::-1])

Data=["Sarat",10,10.0,True,"Pune","India"]
print(Data[10:2:-2])

'''
0   1   2   3   4   5   6   7   8   
10  20  30  40  50  60  70  80  90
-9  -8  -7  -6  -5  -4  -3  -2  -1
'''
nums=[10, 20, 30, 40, 50, 60, 70, 80, 90]
print (nums[-2::-1])
print (nums[-2::1])   #80, 90
print (nums[-2:1:-1]) #[80, 70, 60, 50, 40, 30]
print (nums[-6:4:1])  #[40]
print (nums[20:4:-1])  #[90, 80, 70, 60]
print (nums[20::-435657567686787878]) # To get the last value of the list
print (nums[20:0:-435657567686787878]) # To get the last value of the list
print (nums[20::-435657567686787878]) # To get the last value of the list
print (nums[-2:1:-1]) #[80, 70, 60, 50, 40, 30]
print (nums[6:-2:1])  #[70]
print (nums[:646456575676:435657567686787878]) # To get the first value of the list


'''
Note: The use of : is not mandatory to use 3 times.
If we use it only once also, then also it will consider it as slicing
With the default value for other two i.e. stop/start/step.
'''