'''1.Reverse list using List Slicing'''
list_slice=['Sarat', 'Kumar', 'Pradhan', 'Engineer', 'Testing', 12345]
list_rev=list_slice[::-1]
print('Reverse of the List values:',list_rev)

'''2.Retrieve sublist using Slicing'''
list_slice=['Sarat', 'Kumar', 'Pradhan', 'Engineer', 'Testing', 12345]
list_sub1=list_slice[1:3:1]
print('Reverse of the List values:',list_sub1)
list_sub1=list_slice[1::1]
print('Reverse of the List values:',list_sub1)
list_sub1=list_slice[:3:1]
print('Reverse of the List values:',list_sub1)
list_sub1=list_slice[10:3:1]
print('Reverse of the List values:',list_sub1)
list_sub1=list_slice[10:3:-1]
print('Reverse of the List values:',list_sub1)
list_sub1=list_slice[10:3:-2]
print('Reverse of the List values:',list_sub1)
list_sub1=list_slice[10:1:-3]
print('Reverse of the List values:',list_sub1)

'''3.Retrive whole list without using loops'''
list_slice=['Sarat', 'Kumar', 'Pradhan', 'Engineer', 'Testing', 12345]
list_total=list_slice[0:6:1]
print('Retrieve the complete list:',list_total)

'''4.Print string without using loops'''
list_slice=['Sarat', 'Kumar', 'Pradhan', 'Engineer', 'Testing', 12345]
list_total=list_slice[0:6:1]
print('Retrieve the complete list:',list_total)

str=('SaratKumarPradhan')
print(str)
str_rev=(str[::-1])
print('Reverse of the String is:',str_rev)

'''5.Reverse the string without using loops'''
list_slice=['Sarat', 'Kumar', 'Pradhan', 'Engineer', 'Testing', 12345]
list_total=list_slice[0:6:1]
print('Retrieve the complete list:',list_total)

str=('SaratKumarPradhan')
str_rev=(str[::-1])
print('Reverse of the String is:',str_rev)

'''6.Take numbers in list [10,20,30,40,50,60]'''
'''Print the addition of every alternate number from list'''
list_num=[10,20,30,40,50,60]
list_alt=list_num[::2]
print(list_alt)
sum=0
for i in range(len(list_alt)):
    print(list_alt[i])
    sum=sum+(list_alt[i])
print('Sum of Alterate number is:',sum)




