''' LIST '''
NameOfStudents=["Sarat", "Kumar", "Raj", "Kishore", "Pradhan"]
print(len(NameOfStudents))

print("Name of 1st Student", NameOfStudents[0])
print("Name of last Student", NameOfStudents[4])
print("Name of last Student", NameOfStudents[len(NameOfStudents)-1])

#append
NameOfStudents.append("Rose")
print("Name of last Student after append",NameOfStudents)

NameOfStudents.insert(2,"Lily")
print("Name of last Student after insert",NameOfStudents)

NameOfStudents[0]="SaratSarat"
print("Name of last Student after update",NameOfStudents)

print(NameOfStudents[2][1])

NameOfStudents[3]="Raj"
print("Name of last Student after update",NameOfStudents)

#in operator
print("Sarat" in NameOfStudents)
print("SaratSarat" in NameOfStudents)

if "Sarat" in NameOfStudents:
    print("Name exist")
else:
    NameOfStudents.append("Sarat")

print("Name of last Student after update",NameOfStudents)

# An Incorrect value in index will give error
try:
    idx=NameOfStudents.index("Sarat")
    print(idx)
    NameOfStudents[idx]="Sarat123"
except ValueError:
    print("element value not present in list")


#POP will remove the element of a specific index
#POP also return that element
#Parameter in POP is not mandatory. If not mentioned, then it will return the last index value

print("Name of last Student after update", NameOfStudents.pop())
print("Name of last Student after update", NameOfStudents)

# Difference between Clear and Delete
lst=[13123,23432,5345]
print(lst)

lst.clear()
print(lst)
lst.append(100000)
print(lst)

del lst
lst=[]
lst.append(100000)
print(lst)