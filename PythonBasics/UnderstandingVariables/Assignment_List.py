
'''1.Print items from simple list'''
value_list=['Sarat', 'Kumar', 'Pradhan', 'Engineer', 'Testing', 12345]
print(value_list)

'''2.Print items from Nested List'''
value_list=['Sarat', ['Kumar', ['Pradhan',[0, 1, 2], 'Engineer'], 'Testing'], 12345]
'''To print value ['Pradhan',[0, 1, 2], 'Engineer']'''
print(value_list[1][1])
''' To print value Testing'''
print(value_list[1][2])
''' To print value 1'''
print(value_list[1][1][1][1])
''' To print value Engineer'''
print(value_list[1][1][2])

'''3.Print items from list in reverse order'''
stmt_rev=['Engineer', 'an', 'am', 'I']
print(stmt_rev[::-1])

stmt_rev='reenignE na ma I'
print(stmt_rev[::-1])

'''4.Insert item in list'''
value_list=['Sarat', 'Kumar', 'Pradhan', 'Engineer', 'Testing', 12345]
value_list.insert(4, 'insert_newvalue')
print(value_list)

'''5.Delete item from list and print the removed item'''
print("Remove item name is", value_list.pop(4))
print(value_list)
value_list.insert(4, 'insert_newvalue1')
print(value_list)
value_list.remove("insert_newvalue1")
print(value_list)

'''6.Check in a given Name is present in List of Names and if it exists then print index at which it exists.'''
value_list=['Sarat', 'Kumar', 'Pradhan', 'Engineer', 'Testing', 12345]
idx=value_list.index("Engineer")
print('index value of the value Engineer is:',idx)

'''9.Print elements from Dictionary'''
dict={"first":"1", "2nd":"2", "3rd":"3"}
print(dict.values())

'''10.Count number of item in list'''
value_list=['Sarat', 'Kumar', 'Pradhan', 'Engineer', 'Testing', 12345]
length= len(value_list)
print('length of the list is:', length)

'''11.Print Key and value pair from Dictionary'''
dict={"first":"1", "2nd":"2", "3rd":"3"}
print(dict)
'''It will print only value from the key:value pair'''
print(dict.values())

'''12.Take a dictionary having values as string,print Strings in reverse order'''
dict_rev={"first":"Sarat", "2nd":"Kumar", "3rd":"Pradhan", "4th":"Testing"}
print(dict_rev.values())
val=dict_rev.values()
dict_val=list(val)
print(dict_val)
print(dict_val[1])
idx=len(dict_val)
print(idx)
'''
# the below code is not working to reverse it
print("printing list in reverse order")
while idx in range(0,(len(dict_val)-1)):
    print(dict_val[idx])
    idx=idx-1
'''
print(dict_val[::-1])

print("printing list in reverse order")
print(range(len(dict_val)-1))
idx=len(dict_val)-1
while idx in range(len(dict_val)-1,0,-1):
    print(dict_val[idx])
    idx=idx-1

while idx in range(0,len(dict_val)-1):
    print(dict_val[idx])
    idx=idx-1