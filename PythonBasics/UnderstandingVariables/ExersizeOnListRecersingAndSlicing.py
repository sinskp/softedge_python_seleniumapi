#1.Take a string and print initial 4 characters using slicing
str='sarattesting'
print(str[0:4:1])

#2.Take a string of 10 characters and print characters from Index 2 to 8
str='sarattesting'
print(str[2:9:1])

#3.Reverse content of string using list
str='sarattesting'
print(str[::-1])
print(str[len(str)-1::-1])

#4.Take a string delete few characters from middle
str='sarattesting'
str_after_del=str.replace('tes','')
print(str_after_del)

#5.Take a string and print substring in reverse order
str='sarattesting'
print(str[6:1:-1])

#6.Take a string ,create another string having few characters from source string using slicing
str='sarattesting'
str1=(str[1:100:2])
print(str1)

#7.Take a string,update few initial characters with different characters
# The string is immutable, so we can change the value and assign it to another new variable.
Name='Pradhan'
print(Name)
name1=Name.replace('Pra','PSA')
name1=Name.replace('d','D')
name1=Name.replace('r','s')
name1=Name.replace('Pra','PSA')
print(name1)
