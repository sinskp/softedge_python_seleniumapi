list1=["Sarat", 10, 10.0, False]
print(list1[0])
person={"Name":"Sarat",1000:"Emp no","IsEmployed":False,True:"true",90.00:"Marks"}

# To access data from Dictionary, we need to use Key
print(person["Name"])
print(person[1000])

#print all keys in one line
print("All the keys from Dictionary",person.keys())
#print all keys one by one
personkeys=person.keys()
for k in personkeys:
    print(k)

#print all values in one line
print("All the values from Dictionary",person.values())
#print all values one by one
personkeys=person.keys()
for k in personkeys:
    print("printing values one by one by keys",person[k])

#Another way to print all values one by one
personval=person.values()
for v in personval:
    print("printing values one by one by values",v)


PersonItems=person.items()
'''
personitems="Name":"Sarat",
            1000:"Emp No",
            "Is Employee":False,
            True:"True",
            90.00:"MARKS"
'''

for k,v in PersonItems:
    print(k,end="=")
    print(v)

print("A content of Name key before updating is ", person["Name"])
person["Name"]="NewName"
print(person["Name"])
print("A contnt of name after updating is", person["Name"])

#get method accept the key name value of which could be returned
print(person.get(1000))
person={"Name":"Sarat",1000:"Emp no","IsEmployed":False,True:"TRUE",90.00:"Marks"}
#POP will remove a particilar item based on key which is sent in as an argument to POP
person.pop(True)
print(person)

person={"Name":"Sarat",1000:"Emp no","IsEmployed":False,True:"TRUE",90.00:"Marks"}
person.popitem() #remove last item

person.clear() # It clears everything except memory allocated to it.
print(person)

del person #remove everything including memory also
'''del person{"Name"} #removing one key from the dictonary'''

#We can check the existence of a particular key using IN operaor.
#Key is present in will return boolean TRUE else will return boolean false
person={"Name":"Sarat",1000:"Emp no","IsEmployed":False,True:"true",90.00:"Marks"}
print("Name" in person.keys())
