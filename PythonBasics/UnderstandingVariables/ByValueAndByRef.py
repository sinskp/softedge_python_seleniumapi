list1=["Sarat", "Kumar", 10.0, True]
list2=list1
print("List 1 : ", list1)
print("List 2 : ", list2)

list1[0]=1000
print("Changed the content of the list", list1)
print("Changed the content of the list", list2)

''' 
In the above, the memory location(address) is copied for both list
So, if we change anything to list1 will change to list2.
'''
''' 
In the below, the contents of the index is copied to the new list.
So, if we change anything to list1 will not change to list.
'''
list1=["Sarat", "Kumar", 10.0, True]
list3=list1.copy()
list1[0]="PD"

print("List 1 : ", list1)
print("List 3 : ", list3)

list1=["Sarat", "Kumar", 10.0, True]
list3=list1.copy()
list3[0]="PD"

print("List 1 : ", list1)
print("List 3 : ", list3)

'''
Here we are assigning the address 1st and then contents.
So, the contents will overwrite with the address.
'''

list4=[100, 200, 300]
list5=list4
list5=list4.copy()

list4[0]=100000
print("List 4 : ", list4)
print("List 5 : ", list5)

'''
Here we are assigning the contents 1st and then address.
So, the address will overwrite with the contents.
'''

list4=[100, 200, 300]
list5=list4.copy()
list5=list4

list4[0]=100000
print("List 4 : ", list4)
print("List 5 : ", list5)

