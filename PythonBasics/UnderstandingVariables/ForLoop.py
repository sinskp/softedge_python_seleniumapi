lst=[10, 20, 30, 40]

print("Accessing List members using for loop")
for l in lst:
    print(l)

name="sarat"

print("printing starting using for loop")
for ch in name:
    print(ch)
    print(ch, end='')

print("printing starting using for loop")
for ch in name:
    print(ch, end='')

dict={"name":"Sarat","lastname":"Pradhan", 10:"Age", True:"IsEmployed"}
ks=dict.keys()
for K in ks:
    print(K)
print("printing dictionary values using keys in for loop")
for K in ks:
  print(dict[K])

#"name":"Sarat","lastname":"Pradhan", 10:"Age", True:"IsEmployed"
AllItems=dict.items()

print("printing items from dictionary using for loop")
for Key,Value in AllItems:
    print(Key,':',Value)

print("Printing numbers using for loop and range function")
Numbers=range(1,11)
for num1 in Numbers:
    print(num1)

#The below statement will go infinite if we use a valid value
#for num1= and if we miss to use the increament process i.e. num1=num1+1
num1=10
while num1 in Numbers:
    print("I am in while loop")
    num1=num1+1

# The below is similar to Do While loop
num1=1
while True:
    print("I am inside loop")
    if not num1 in Numbers:
        break;
    num1=num1+1

# The below is similar to Do While loop
num1=1000
while True:
    print("I am inside loop for once")
    if not num1 in Numbers:
        break;
    num1=num1+1

