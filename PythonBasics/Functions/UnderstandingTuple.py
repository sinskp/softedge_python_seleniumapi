'''
We should use tuple for data integrity purpose. data shouldn't be tampered
while fetching data from database and save it in a tuple
because tuple data values are immutable
However, List is mutable
tuple is enclosed by brace bracket
but List enclosed by square bracket
'''
t=("Pranoday",10,10.0,True)

print("Accessing 1st element from Tuple",t[0])

print("Accessing all elements from tuple 1 by 1")
for mem in t:
    print(mem)

print("Number of elements in tuple are ",len(t))
i=0

print("Printing tuple elements using While loop")
while i in range(0,len(t)):
    print(t[i])
    i=i+1

print("Spreading of tuples")
var1,var2,var3,var4=t
print(var1)
print(var2)
print(var3)
print(var4)