'''
For Method Overriding:
The method name, parameter list and data types should be the same in
both patent and child class.
For Overriding: The child class method IMPLEENTATION
will come in to picture

'''

from BankAccount.Accounts import Account

class SalaryAccount(Account):

    def __init__(self,A,B):
        super().__init__(A,B)
        print("I am in Saving Account Constructor")

    def CloseAccount(self):
        print("When person leaves the organizarion, Account will get closed")

    def Substraction(self,num1, num2):
        print("Substraction with two arguments")