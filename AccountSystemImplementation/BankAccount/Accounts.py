from abc import ABC, abstractmethod
from typing import final, Final

from multipledispatch import dispatch

from BankAccount.InsufficientBalanceException import InsufficientBalanceException

class Account(ABC):
    '''
    def __init__(self,actno, Bal)
    print("I am in account class Constructor")
    '''
    INTERESTRATE:Final=10.0

    @dispatch(int, int)
    def __init__(self, ActNo, B):
        self.AccountNo=ActNo
        self.Balance=B
    @dispatch(int)
    def __init__(self, ActNo):
        self.AccountNo=ActNo
        self.Balance=0

    def DepositAmount(self, AmountTobeDeposited):
        self.Balance=self.Balance+AmountTobeDeposited

    def WithdrawAmount(self, AmountTobeWithdrawn):
        if self.Balance<AmountTobeWithdrawn:
            raise InsufficientBalanceException()
        self.Balance = self.Balance

    @abstractmethod
    def CloseAccount(self):
        pass

    @dispatch(int,int)
    def sum(self, num1, num2):
        c=num1+num2
        print("Addition of 2 number is "+str(c))

    @dispatch(str,str)
    def sum(self,str1,str2):
        c= str1 + str2
        print("Concatenation of 2 string is" + c)

    @dispatch(str, str, str)
    def sum(self,str1,str2,str3):
        c= str1 + str2 + str3
        print("Concatenation of 3 string is" + c)

    @dispatch(int)
    def sum(self,num1,num2):
        c=num1+num2
        print("Addition of 2 number Int and Float is" + str(c))

    @dispatch(int)
    def sum(self,a):
        print("Sum with 1 arg")

    @final
    def OpenAcount(self):
        print("Please submit KYC documents.Account Opening may take 8 working days")















    def __init__(self,AcctNo,Bal):
        print("I am in Accout Class constructor")
        self.AccountNo=AcctNo
        self.balance=Bal

    def DepositAmount(se,AmountToBeDeposited):
        se.balance=se.balance+AmountToBeDeposited

    #def GetBalance(self):
    #    return self.balance

    def WithdrawAmount(self,AmountToBeWithdrawn):
       # self.balance=self.balance-AmountToBeWithdrawn
        if self.balance < AmountToBeWithdrawn:
            raise InsufficientBalanceException()
        self.balance=self.balance - AmountToBeWithdrawn

    def GetBalance(self):
        return self.balance






