class InsufficientBalanceException(Exception):
    ExceptionMessage="You have insufficient balance in your account"