'''
The class name InsufficientBalanceException should be used in
Accounts.py with the RAISE keyword and the same InsufficientBalanceException
should be used with the Exception keyword in AccountSystemDemo.py
'''

from BankAccount.Accounts import Account
from BankAccount.InsufficientBalanceException import InsufficientBalanceException
from TypesOfAccounts.SalaryAccount import SalaryAccount
from TypesOfAccounts.SavingsAccount import SavingsAccount


Sact1=SavingsAccount(3,300.0)

Sact1.DepositeAmount(5000)
try:
    Sact1.WithdrawAmount(5300)
    CurrentBalance = Sact1.GetBalance()
    print("After Withdrawal of money Balance of Account No:" + str(Sact1.AccountNo) + " is " + str(CurrentBalance))
except InsufficientBalanceException:
    print(InsufficientBalanceException.ExceptionMessage + str(Sact1.AccountNo))

    Sact1.WithdrawAmount(300)
    Sact1.DepositAmount(2000)
    Sact1.CloseAccount()

Sal1=SalaryAccount(4,5)
Sal1.DepositAmount(5000)
try:
    Sal1.WithdrawAmount(5300)
    CurrentBalance = Sal1.GetBalance()
    print("After withdrawal of money Balance of Account No: " + str(Sal1.AccountNo) + " is " + str(CurrentBalance))
except InsufficientBalanceException:
    print(InsufficientBalanceException.ExceptionMessage + str(Sal1.AccountNo))

    Sal1.WithdrawAmount(100)
    Sal1.DepositAmount(2000)
    Sal1.CloseAccount()
    Sal1.sum("Sarat", "Kumar", "Pradhan")
    Sal1.sum(10,20)
    Sal1.sum(10, 200)
    Sal1.sum(1000)



# Commenting because of abstract changes to the parent ACCOUNTS class
'''
Acct1=Account(1,1000)
Acct1.DepositAmount(2000.00)

try:
    Acct1.WithdrawAmount(55500.00)
    CurrentBalance = Acct1.GetBalance()
    print("After Withdrawal of money balance of account no:" + str(Acct1.AccountNo) + " is " + str(CurrentBalance))

except InsufficientBalanceException:
    print(InsufficientBalanceException.ExceptionMessage + " "  + str(Acct1.AccountNo))

Acct2=Account(2,2000)
Acct2.DepositAmount(12000.00)

IBException=InsufficientBalanceException()

try:
    Acct2.WithdrawAmount(1000.00)
    CurrentBalance=Acct2.GetBalance()
    print("After Withdrawal of money balance of account no:" + str(Acct2.AccountNo) + " is " + str(CurrentBalance))

except InsufficientBalanceException:
    print(IBException.ExceptionMessage + " "  + str(Acct2.AccountNo))

print("+++++++++++++++++++++++++++++++++++++++++")
print("Starting of child,parent,default cnstructor")
print("+++++++++++++++++++++++++++++++++++++++++")

Sact1=SavingsAccount(3,300,5)

Sact1.DepositAmount(5000)
Sact1.WithdrawAmount(2000)
CurrentBalance=Sact1.GetBalance()
print("After Withdrawal of money balance of account no:" + str(Sact1.AccountNo) + " is " + str(CurrentBalance))
'''
















### Other Way using object
'''
IBException=InsufficientBalanceException()

try:
    Acct1.WithdrawAmount(55500.00)
    CurrentBalance = Acct1.GetBalance()
    print("After Withdrawal of money balance of account no:" + str(Acct1.AccountNo) + " is " + str(CurrentBalance))

except InsufficientBalanceException:
    print(IBException.ExceptionMessage)
'''


'''
Acct1=Account(1,1000)
#Acct1.AccountNo=10
#Acct1.balance=10000
Acct1.WithdrawAmount(5000)

Acct1.DepositAmount(2000.00)
Acct2=Account(2,2000)
Acct2.WithdrawAmount(1000.00)
CurrentBalance=Acct2.GetBalance()

print(Acct1.AccountNo)
print(Acct2.AccountNo)
print(Acct1.AccountNo)
print(Acct2.balance)
print(Acct1.balance) # Print after add, based on the method

#print("After withdrawal the balance of the account no "+ str(Acct1.AccountNo) + "is" + str(Acct1.WithdrawAmount))
print("After withdrawal the balance of the account no "+ str(Acct1.AccountNo) + " is " + str(Acct1.balance))
print("After withdrawal the balance of the account no "+ str(Acct2.AccountNo) + " is " + str(Acct2.GetBalance()))
print("After withdrawal the balance of the account no "+ str(Acct2.AccountNo) + " is " + str(CurrentBalance))
print("After withdrawal the balance of the account no "+ str(Acct2.AccountNo) + " is " + str(Acct2.balance))
'''







